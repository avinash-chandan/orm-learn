package com.cognizant.ormlearn.model;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author AVINASH
 *
 */

@Table(name = "attempt_question")
@Entity
public class AttemptQuestion {

	@Id
	@Column(name = "aq_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@JoinColumn(name = "aq_at_id")
	private Attempt attempt;

	@ManyToOne
	@JoinColumn(name = "aq_qt_id")
	private Question question;

	@OneToMany(mappedBy = "attemptQuestion", fetch = FetchType.EAGER)
	private Set<AttemptOption> attemptOptionList;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Attempt getAttempt() {
		return attempt;
	}

	public void setAttempt(Attempt attempt) {
		this.attempt = attempt;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Set<AttemptOption> getAttemptOptionList() {
		return attemptOptionList;
	}

	public void setAttemptOptionList(Set<AttemptOption> attemptOptionList) {
		this.attemptOptionList = attemptOptionList;
	}

	public AttemptQuestion() {
		// TODO Auto-generated constructor stub
	}

	public AttemptQuestion(int id, Attempt attempt, Question question) {
		super();
		this.id = id;
		this.attempt = attempt;
		this.question = question;
	}

	@Override
	public String toString() {
		return "AttemptQuestion [id=" + id + ", attempt=" + attempt + ", question=" + question + ", attemptOptionList="
				+ attemptOptionList + "]";
	}

}
